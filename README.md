# Kelvin backend technical test

## [/client](https://gitlab.com/bhoyebalde/kelvintest/-/tree/master/client)

Contient 
* La calcutrice
* Les pages admins faites version `React.js`

Technologies utilisées

* React.js
* Bootstrap 4
* Redux
* Whatwg-fetch

## [/api](https://gitlab.com/bhoyebalde/kelvintest/-/tree/master/api)

Contient
* Les APIs, lien de la documentation et du endpoint (http://api-url/api)
* Les pages admins version `Symfony/twig`

Technologies utilisées

* Symfony 5
* Bootstrap 4
* API Plateform
* Webpack encore


## Prérequis
- PHP >= 7.2.5
- Un serveur web (j'utiliserai [Symfony Local Web Server](https://symfony.com/doc/current/setup/symfony_server.html) dans ce tuto d'installation)
- Une base de donnée
- [Composer](https://getcomposer.org/)
- NodeJS
- Yarn ou npm (qui vient avec NodeJs)
- git

## Installation

`git clone https://gitlab.com/bhoyebalde/kelvintest.git`

Tous les deux dossiers contiennent un `readme` qui explique leur installation et configuration.