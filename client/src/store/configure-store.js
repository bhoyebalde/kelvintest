import { createStore, applyMiddleware } from 'redux';

import {rootReducer,initialState} from './../app/reducers/index';

import expenseMiddleware from './../app/middleware/expenseMiddleware'

export const configureStore = () => createStore(
        rootReducer,initialState, 
        applyMiddleware(
            expenseMiddleware
        )
    )

export default configureStore;