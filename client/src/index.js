import React from 'react';
import ReactDOM from 'react-dom';

import Root from './root';
import * as serviceWorker from './serviceWorker';

import './index.css'

import {configureStore} from './store/configure-store';
import { Provider } from 'react-redux';

const store = configureStore();

ReactDOM.render(
    <Provider store = {store}>
        <React.StrictMode>
            <Root />
        </React.StrictMode>
    </Provider>
    ,document.getElementById('root')
);

serviceWorker.unregister();
