import React, { Component } from 'react'

import App from './../app/App'
import Expense from './../app/components/expense/'
import Calculator from './../app/components/calculator/'

import {
  BrowserRouter as Router,
    Switch,Route
  } from 'react-router-dom'
  
class Root extends Component {

  render() {
    return (
      <Router>
          <Switch>
              <Route path="/calculator" component={Calculator} />            
              <Route path="/admin" component={Expense} />            
              <Route path="/" component={App} />
          </Switch>
      </Router>
    )
  }
}  

export default Root
