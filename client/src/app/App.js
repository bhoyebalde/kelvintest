import React from 'react'
import { Container,Row,Col,Card } from 'react-bootstrap'
import {
  Link
} from "react-router-dom"

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {connect} from 'react-redux';

import { getExpenses } from './actions/actionsExpense';

class App extends React.Component {

  componentDidMount(){
    this.props.initExpenses()
  }

  render() {
    return (
      <Container className="container">
        <Row className="justify-content-md-center">
            <Col xs={12}>
                <h3 className="text-center">Carbon footprint calculation technology</h3>
            </Col>          
            <Col xs={12}>
                <Row>
                  <Col xs={6}>
                      <Card className="text-center">
                        <Card.Body>
                          <Card.Title>Calculator</Card.Title>
                          <Card.Text>
                            Select your Expense type and type its amount to calculate your carbon footprint
                          </Card.Text>
                          <Link  className="btn btn-primary" to="/calculator">Calculator</Link>
                        </Card.Body>
                      </Card>
                  </Col> 
                  <Col xs={6}>
                      <Card className="text-center">
                        <Card.Body>
                          <Card.Title>Admin</Card.Title>
                          <Card.Text>
                            List, add, edit and delete any expense type and its associated carbon footprint ratio
                          </Card.Text>
                          <Link to="/admin" className="btn btn-primary">Admin</Link>
                        </Card.Body>
                      </Card>                    
                  </Col> 
                </Row>
            </Col>
        </Row>   
      </Container>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  initExpenses: (expense) => dispatch(getExpenses(expense))
})

export default connect(null,mapDispatchToProps)(App)
