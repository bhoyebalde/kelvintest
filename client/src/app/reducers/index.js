import {combineReducers} from 'redux'

import * as expenseReducer from './expenseReducer'
import * as calculReducer from './calculReducer'
import * as generalReducer from './generalReducer'

export const rootReducer = combineReducers({
    expense : expenseReducer.reducer,
    calcul : calculReducer.reducer,
    general : generalReducer.reducer
})

export const initialState = {
    expense : expenseReducer.initialState,
    calcul : calculReducer.initialState,
    general : generalReducer.initialState
}