import * as types from '../constants/types'

export const initialState = {
    isFetching: false,
    error: ''
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case types.TOGGLE_LOADING:{
            return ({...state, isFetching : !state.isFetching})
        }        
        case types.ACTION_SET_ERROR:{
            return ({...state, error : action.payload})
        }
        default:
            return state
    } 
}