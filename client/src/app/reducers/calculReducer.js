import * as types from '../constants/types'

export const initialState = {
    result : 0
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case types.ACTION_CALCUL:{
            return ({...state, result : action.payload.resultat})
        }
        default:
            return state
    } 
}