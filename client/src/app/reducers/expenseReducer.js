import * as types from '../constants/types'
import update from 'immutability-helper'

export const initialState = {
    expenses : []
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case types.ACTION_ADD_EXPENSE:{
            return ({...state, expenses : [...state.expenses,{...action.payload}]})
        }        
        case types.ACTION_DELETE_EXPENSE:{
            return ({...state, expenses : update(state.expenses, arr => arr.filter(item => item.id !== action.payload.id) )})
        }        
        case types.ACTION_UPDATE_EXPENSE:{
            return ({...state, expenses : update(state.expenses, arr => arr.map(item => item.id === action.payload.id ? {...action.payload,id : item.id} : item) )})
        }
        case types.ACTION_SET_EXPENSES: {
            return (
                {...state, expenses : action.payload}
            )
        }
        default:
            return state
    } 
}