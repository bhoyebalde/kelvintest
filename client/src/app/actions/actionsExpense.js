import * as types from '../constants/types'

const host = process.env.REACT_APP_API_URL

export const addNewExpenseAction = (expense) => (
{
    type: types.ACTION_ADD_EXPENSE,
    payload: expense,
    meta: {
        type: 'api',
        url: host + '/expenses',
        method: 'POST',
        body: JSON.stringify(expense)
    }
})

export const getExpenses = () => (
{
    type: types.ACTION_SET_EXPENSES,
    meta: {
        type: 'api',
        url: host + '/expenses',
        method: 'GET',
        Accept: 'application/json'
    }
})

export const deleteExpenseAction = (expense) => (
{
    type: types.ACTION_DELETE_EXPENSE,
    payload: expense,
    meta: {
        type: 'api',
        url: host + `/expenses/${expense.id}`,
        method: 'DELETE'
    }     
})

export const updateExpenseAction = (expense) => (
{
    type: types.ACTION_UPDATE_EXPENSE,
    payload: expense,
    meta: {
        type: 'api',
        url: host + `/expenses/${expense.id}`,
        method: 'PUT',
        body: JSON.stringify(expense)
    }    
})