import * as types from '../constants/types'

const host = process.env.REACT_APP_API_URL

export const calculAction = (params) => (
    {
        type: types.ACTION_CALCUL,
        payload: params,
        meta: {
            type: 'api',
            url: host + '/calculator',
            method: 'POST',
            body: JSON.stringify(params)
        }
})