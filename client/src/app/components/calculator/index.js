import React from 'react'
import { Form, Card, Button, Row, Col, Container, Spinner, Alert } from 'react-bootstrap'
import {
    Link
} from "react-router-dom"
import './index.css'

import { connect } from 'react-redux';
import { getExpenses } from './../../actions/actionsExpense';
import { calculAction } from './../../actions/actionsCalculator';

class Calculator extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            types: [],
            expenseType: "?",
            amount: 1,
            updating: false,
            validate: false,
            resultat: 0,
            isFetching: false,
            error: ""
        }

        this.handleTypeChangeChange = this.handleTypeChangeChange.bind(this)
        this.handleAmountChange = this.handleAmountChange.bind(this)
    }

    componentDidMount() {
        this.props.initExpenses()
    }

    static getDerivedStateFromProps(props, state) {

        if (props.types !== state.types) {
            return {
                types: props.types,
                expenseType: props.types.length > 0 ? props.types[0].id : state.expenseType
            }
        }

        if (props.resultat !== state.resultat) {
            return {
                resultat: props.resultat
            }
        }

        if (props.error !== state.error) {
            return {
                error: props.error
            }
        }

        if (props.isFetching !== state.isFetching) {
            return {
                isFetching: props.isFetching
            }
        }

        return null;
    }

    handleTypeChangeChange(event) {
        this.setState({ expenseType: event.target.value })
    }

    handleAmountChange(event) {
        this.setState({ amount: event.target.value })
    }

    handleSubmit = event => {
        event.preventDefault()

        const form = event.currentTarget

        if (form.checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
        }

        this.setState({
            validated: true
        })

        console.log(this.state.expenseType)
        this.props.calcul(
            {
                expenseType: parseInt(this.state.expenseType),
                amount: parseInt(this.state.amount)
            }
        )
    }

    render() {

        const { types, amount, validated, resultat, expenseType, error, isFetching } = this.state
        return (
            <Container>
                <Row className="justify-content-md-center">

                    <Col xs={12}>
                        <Card>
                            <Card.Body>
                                <Card.Title>Calculator</Card.Title>
                                {error !== '' ?
                                    <Col xs={12} className="title">
                                        <Alert variant="danger">
                                            {error}
                                        </Alert>
                                    </Col>
                                : ``}
                                {isFetching ?
                                    <Spinner size="sm" animation="border" role="status"></Spinner>
                                : ``}
                                <Form noValidate validated={validated} onSubmit={this.handleSubmit}>

                                    <Form.Group>
                                        <Form.Label>Expense type</Form.Label>
                                        <Form.Control onChange={this.handleTypeChangeChange} as="select" size="md" value={expenseType} custom>
                                            {
                                                types.map(item => {
                                                    return <option key={item.id} value={item.id}>{item.expenseType}</option>
                                                })
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group>
                                        <Form.Label>Amount (€)</Form.Label>
                                        <Form.Control
                                            required
                                            type="number"
                                            min="1"
                                            value={amount}
                                            onChange={this.handleAmountChange}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            Ratio
                                            </Form.Control.Feedback>
                                    </Form.Group>

                                    <Button type="submit" variant="primary">
                                        {`Calculate`}
                                    </Button>
                                </Form>

                            </Card.Body>
                        </Card>
                    </Col>
                    {resultat !== 0 ?                     
                        <Col xs={12} className="result">
                            <Card>
                                <Card.Body>Yor CO2 footprint for this expense: <b>{resultat / 1000} kg</b></Card.Body>
                            </Card>
                        </Col>
                    :
                    ``}
                    <Col xs={12} className="title">
                        <Link to="/" className="btn btn-secondary">Retourner à l'accueil</Link>
                    </Col>
                </Row>
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    return ({
        types: state.expense.expenses,
        resultat: state.calcul.result,
        error: state.general.error,
        isFetching: state.general.isFetching
    })
}

const mapDispatchToProps = dispatch => ({
    initExpenses: (expense) => dispatch(getExpenses(expense)),
    calcul: (params) => dispatch(calculAction(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Calculator)