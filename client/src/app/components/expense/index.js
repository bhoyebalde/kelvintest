import React from 'react'

import { Col,Row,Container,Spinner,Alert } from 'react-bootstrap'

import AddExpense from './add'
import ExpensesList from './list'
import {
    Link
  } from "react-router-dom"
import './index.css'
import {connect} from 'react-redux';

class Expense extends React.Component {

    constructor(props){
        super(props)
        
        this.state = {
            expense : null,
            isFetching: false,
            error: ''
        }
    }
 
    static getDerivedStateFromProps(props, state) {

        if (props.isFetching !== state.isFetching) {
            return {
                isFetching : props.isFetching
            };
        }
    
        if (props.error !== state.error) {
            return {
                error : props.error
            }
        }
    
        return null;
    }

    onUpdateExpense = (expense) => {
        this.setState({
            expense: expense
        })
    }
    
    onExpenseUpdated = () => {
        this.setState({
            expense: null
        })
    }

    render() {
        const {expense,isFetching,error} = this.state

        return (
            <Container>
                <Row>
                    <Col xs={12} className = "title">
                        <h3>Admin - Crud - Expense </h3>
                        { isFetching ? <Spinner animation="border" role="status"></Spinner>  : ``}
                    </Col>     

                    { error !== '' ?                
                        <Col xs={12} className = "title">
                            <Alert variant="danger">
                                { error }
                            </Alert>
                        </Col>
                     : `` }

                    <Col xs={12} className = "expenses-add">
                        <AddExpense newExpense = {expense}  onExpenseUpdated = {this.onExpenseUpdated }  />
                    </Col>        
                    <Col xs={12}  className = "expenses-list">
                        <ExpensesList onUpdateExpense = { (expense) => this.onUpdateExpense(expense) }  />
                    </Col>
                    <Col xs={12} className = "title">
                        <Link to="/" className="btn btn-secondary">Retourner à l'accueil</Link>                        
                    </Col>                      
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        isFetching : state.general.isFetching,
        error : state.general.error
    })
}

export default connect(mapStateToProps,null)(Expense)
