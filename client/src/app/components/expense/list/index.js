import React from 'react';

import {Table,Col,Row,Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit,faTrash } from '@fortawesome/free-solid-svg-icons'
import { deleteExpenseAction } from './../../../actions/actionsExpense';
import { getExpenses } from './../../../actions/actionsExpense';

import './index.css'

class ExpensesList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            expenses : []
        }
    }
  
    componentDidMount(){
        this.props.initExpenses()
    }
    
    static getDerivedStateFromProps(props, state) {

        if (props.expenses !== state.expenses) {
            return {
                expenses : props.expenses
            };
        }
    
        return null;
    }

    handleUpdateClick = (event) => {
        this.props.onUpdateExpense(event)
    }

    handleDeleteClick = (event) => {
        this.props.deleteExpense(event)
    }

    render() {
        const {expenses} = this.state

        return (
            <Row className="justify-content-md-center">
                <Col xs={12}>
                    <h3 className="titleExpense">List</h3>
                </Col>        
                <Col xs={12}>
                    <Table striped bordered hover responsive>
                        <thead>
                            <tr>
                                <th>Expense Type</th>
                                <th>Ratio</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                expenses.map(expense => {

                                    return (
                                        <tr key = {expense.id}>
                                            <td>{expense.expenseType}</td>
                                            <td>{expense.ratio}</td>
                                            <td>
                                                <Button variant="secondary" onClick = {() => this.handleUpdateClick(expense) }> <FontAwesomeIcon icon={faEdit} /></Button>
                                                <Button className = "btnSup" onClick = {() => this.handleDeleteClick(expense) }variant="danger"> <FontAwesomeIcon icon={faTrash} /></Button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        )
    }
}

ExpensesList.defaultProps = {
    expenses : []
};

ExpensesList.propTypes = {
    expenses : PropTypes.array
};

const mapStateToProps = (state) => {
    return({
        expenses : state.expense.expenses
    });
}

const mapDispatchToProps = dispatch => ({
    deleteExpense: (expense) => dispatch(deleteExpenseAction(expense)),
    initExpenses: (expense) => dispatch(getExpenses(expense))    
})

export default connect(mapStateToProps,mapDispatchToProps)(ExpensesList)