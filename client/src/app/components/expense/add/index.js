import React from 'react'

import { Form,Card,Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

import { addNewExpenseAction } from './../../../actions/actionsExpense'
import { updateExpenseAction } from './../../../actions/actionsExpense'

class AddExpense extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            id : props.id,
            expenseType : props.expenseType,
            ratio: props.ratio,
            updating: false,
            validate: false            
        }

        this.handleTypeChange = this.handleTypeChange.bind(this)
        this.handleRatioChange = this.handleRatioChange.bind(this)
    }
 
    
    static getDerivedStateFromProps(props, state) {

        if (props.newExpense !== null && props.newExpense.expenseType !== state.expenseType && props.newExpense.ratio !== state.ratio && !state.updating) {
    
            return {
                id : props.newExpense.id,
                expenseType : props.newExpense.expenseType,
                ratio: props.newExpense.ratio,
                updating: true
            }
        }
 
        return null
    }

    handleTypeChange(event) {
        this.setState({expenseType: event.target.value})
    }

    handleRatioChange(event) {
        this.setState({ratio: event.target.value})
    }
    
    handleSubmit = event => {

        event.preventDefault()

        const form = event.currentTarget

        this.setState({
            validated : true
        })    

        if (form.checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
        }
        else
        {
            if (this.state.updating){
                this.props.updateExpense({
                    id : this.state.id,
                    expenseType : this.state.expenseType,
                    ratio : this.state.ratio
                })

                this.props.onExpenseUpdated()
            }
            else{
                this.props.addExpense({
                    expenseType : this.state.expenseType,
                    ratio : parseInt(this.state.ratio)
                })
            }

            this.setState({
                id : '',
                expenseType : '',
                ratio: '',
                validated: false,
                updating : false
            })  
        }    
    }

    handleReset = () => {

        this.setState({
            id : '',
            expenseType : '',
            ratio: '',
            validated: false,
            updating : false
        }) 

        this.props.onExpenseUpdated()
    }

    render() {

        const {expenseType,ratio,validated,updating} = this.state
        return (
            <Card>
                <Card.Body>
                    <Card.Title>{updating ? `Updating` : `Adding`} expense</Card.Title>
             
                        <Form noValidate validated={validated} onSubmit={this.handleSubmit}>
                            <Form.Group>
                                <Form.Label>Expense type * </Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    value={expenseType}
                                    onChange={this.handleTypeChange}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Type
                                </Form.Control.Feedback>
                            </Form.Group>                            
                            <Form.Group>
                                <Form.Label>Ratio * </Form.Label>
                                <Form.Control
                                    required
                                    type="number"
                                    value={ratio}
                                    onChange={this.handleRatioChange}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Ratio
                                </Form.Control.Feedback>                                
                            </Form.Group>   

                            <Button type="submit" variant="secondary">{updating ? `Save` : `Add`}</Button>
                            { updating ? <Button onClick = {this.handleReset} variant="default">Cancel</Button> : '' }
                        </Form>
               
                </Card.Body>
            </Card>
        )
    }
}

AddExpense.defaultProps = {
    expenseType : '',
    ratio: 0,
}

AddExpense.propTypes = {
    expenseType : PropTypes.string,
    ratio: PropTypes.number
}

const mapDispatchToProps = dispatch => ({
    addExpense: (expense) => dispatch(addNewExpenseAction(expense)),
    updateExpense: (expense) => dispatch(updateExpenseAction(expense))
})

export default connect(null,mapDispatchToProps)(AddExpense)
