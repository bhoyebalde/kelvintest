import 'whatwg-fetch'

import * as types from './../constants/types'

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

const expenseMiddleware = (store) => (next) => (action) => {
  if (!action.meta || action.meta.type !== 'api') {
    return next(action);
  }

  const toggleIsLoadingAction = {
    type: types.TOGGLE_LOADING
  }

  var setErrorAction = {
    type: types.ACTION_SET_ERROR,
    payload: ''
  }

  store.dispatch(toggleIsLoadingAction)
  store.dispatch(setErrorAction)

  const { url } = action.meta;
  const fetchOptions = Object.assign({
    headers: {
      'Content-Type': 'application/json'
    },
  }, action.meta);
  console.log(fetchOptions)
  fetch(url, fetchOptions)
    .then(checkStatus)
    .then(resp => resp.json())
    .then(json => {
      let newAction = Object.assign({}, action, {
        payload: json
      })
      delete newAction.meta;

      store.dispatch(toggleIsLoadingAction)
      store.dispatch(newAction);
    })
    .catch(function (error) {
      if (fetchOptions.method === "DELETE") {
        let newAction = Object.assign({}, action, {
          payload: action.payload
        })

        delete newAction.meta;
        store.dispatch(newAction);
      }
      else {
        console.log(error)
        setErrorAction.payload = error.message
        store.dispatch(setErrorAction)
      }

      store.dispatch(toggleIsLoadingAction)
    })
}

export default expenseMiddleware;