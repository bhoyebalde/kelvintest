Kelvin backend technical test

Développeur: Mamadou Balde

# CLIENT - REACT.JS

## Prérequis
- NodeJS
- Yarn ou npm (qui vient avec NodeJs)

## Installation && Configuration

Dans le fichier `client/.env`

* Remplacer la variable d'environnement `REACT_APP_API_URL` par l'url du backend

`REACT_APP_API_URL=http://127.0.0.1:8000/api`

Ouvrir un terminal à la racine du projet (dans `/client`) et executer les commandes suivantes

* `yarn install` ou `npm install`

## Lancement du serveur

* `yarn start`

Naviguer sur `http://127.0.0.1:3000`

![alt text](doc/front-home.PNG)

Cliquer sur `Calcultor` pour les calculs et `Admin` pour acceder aux pages admins faites en react.js

![alt text](doc/front-calculator.PNG)

![alt text](doc/front-admin-page.PNG)
