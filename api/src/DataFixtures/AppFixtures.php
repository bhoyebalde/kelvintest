<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Expense;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $expense = new Expense();
        $expense->setExpenseType("Low-cost flight ticket");
        $expense->setRatio(10000);
        $manager->persist($expense);    
        
        $expense = new Expense();
        $expense->setExpenseType("Regular flight ticket");
        $expense->setRatio(3600);        
        $manager->persist($expense);

        $expense = new Expense();
        $expense->setExpenseType("Electricity");
        $expense->setRatio(6000);
        $manager->persist($expense);

        $expense = new Expense();
        $expense->setExpenseType("Legal advice");
        $expense->setRatio(160);
        $manager->persist($expense);

        $expense = new Expense();
        $expense->setExpenseType("Car gas");
        $expense->setRatio(3200);
        $manager->persist($expense);

        $manager->flush();
    }
}
