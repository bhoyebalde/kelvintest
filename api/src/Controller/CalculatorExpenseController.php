<?php

namespace App\Controller;

use App\Repository\ExpenseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class CalculatorExpenseController extends AbstractController
{
    /**
     * @Route("/calculator", name="calculator", methods={"POST"})
     */
    public function index(Request $request, ExpenseRepository $expenseRepository): JsonResponse
    {
        // Objet de retour 
        $response = [
            'message' => 'success',
            'code' => 200,
            'resultat' => 0
        ];

        // decode le contenu du body
        $content = json_decode($request->getContent());

        // on vérifie que les attributs requis sont fourni
        if (property_exists($content, 'expenseType') && property_exists($content, 'amount'))
        {
            $expenseTypeId = $content->expenseType;
            $amount = (int)$content->amount;
   
            // La somme doit etre >= 0
            if ($expenseTypeId != null && $amount >= 0){

                // On retrouve le type
                $expense = $expenseRepository->find($expenseTypeId);

                if ($expense != null){
                    $result = 0;

                    // calcul
                    $result = $expense->getRatio() * $amount;

                    $response['resultat'] = $result;
                }
                else
                    {
                        $response['message'] = 'Not found';
                        $response['code'] = 404;
                    }
            }
            else
                {
                    $response['message'] = 'Bad Request';
                    $response['code'] = 400;
                }
        }
        else
            {
                $response['message'] = 'Bad Request';
                $response['code'] = 400;
            }

            $jsonResponse = new JsonResponse($response);
            $jsonResponse->setStatusCode($response['code']);

        return  $jsonResponse;
    }
}
