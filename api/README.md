Kelvin backend technical test

Développeur: Mamadou Balde

# BACKEND - PHP/SYMFONY

## Prérequis
- PHP >= 7.2.5
- Un serveur web (j'utiliserai [Symfony Local Web Server](https://symfony.com/doc/current/setup/symfony_server.html) dans ce tuto d'installation)
- Une base de donnée
- [Composer](https://getcomposer.org/)
- NodeJS
- Yarn ou npm (qui vient avec NodeJs)
- git

## Installation && Configuration

### Installation de Symfony Local Web Server

Suivre le tuto de ce lien (https://symfony.com/doc/current/setup/symfony_server.html)

Après l'installation, la commande `symfony --v` devrait retourner la version du serveur

### Configuration

Dans le fichier `api/.env`

* Remplacer la variable d'environnement `DATABASE_URL` par les informations de connexion de votre base de donnée

`DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name`

Ouvrir un terminal à la racine du projet (dans `/api`) et executer les commandes suivantes

* `composer install`

* `yarn install` ou `npm install`

* `yarn run dev` (compilation des assets)

* `php bin/console doctrine:database:create` si la base de données n'existe pas 

* `php bin/console make:migration`

* `php bin/console doctrine:migrations:migrate`

* `php bin/console doctrine:fixtures:load` chargement des données par défault

## Lancement du serveur

* `symfony serve`

![alt text](doc/symfony-server.PNG)

Naviguer sur `http://127.0.0.1:8000` pour acceder au `backend php` et `http://127.0.0.1:8000/api` pour acceder à la documentation des APIs.

![alt text](doc/backend-crud.PNG)

![alt text](doc/backend-api-doc.PNG)